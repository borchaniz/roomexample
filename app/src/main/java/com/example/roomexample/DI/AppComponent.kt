package com.example.roomexample.DI

import android.app.Application
import android.content.Context
import com.example.roomexample.App
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
        modules =  [
            AndroidSupportInjectionModule::class,
            AppModule::class,
            BindingModule::class
        ]
)
interface AppComponent : AndroidInjector<App>{
    @Component.Builder
    interface Builder{
        @BindsInstance
        fun create(app: Context) : Builder

        fun build() : AppComponent
    }
}