package com.example.roomexample.DI

import android.content.Context
import com.example.roomexample.DataBase
import com.example.roomexample.EditUserService
import dagger.Module
import dagger.Provides
import javax.inject.Inject
import javax.inject.Singleton

@Module
class AppModule {

    @Provides
    @Singleton
    fun provideDB(context: Context) : DataBase = DataBase.getInstance(context)!!


//     @Provides
//     @Singleton
//     fun provideEditUserService():EditUserService = EditUserService()
}