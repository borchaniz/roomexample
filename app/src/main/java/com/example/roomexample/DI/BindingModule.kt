package com.example.roomexample.DI

import com.example.roomexample.AddUserService
import com.example.roomexample.EditUserService
import com.example.roomexample.GetUserService
import com.example.roomexample.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module(includes = [AppModule::class])
abstract class BindingModule{
    @ContributesAndroidInjector
    abstract fun mainActivity():MainActivity

    @ContributesAndroidInjector
    abstract fun editUserService():EditUserService

    @ContributesAndroidInjector
    abstract fun addUserService():AddUserService

    @ContributesAndroidInjector
    abstract fun getUserService():GetUserService
}