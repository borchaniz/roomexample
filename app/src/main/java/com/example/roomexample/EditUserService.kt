package com.example.roomexample

import android.app.Service
import android.content.Intent
import android.os.IBinder
import android.util.Log
import com.example.roomexample.Entities.User
import dagger.android.DaggerIntentService
import dagger.android.DaggerService
import javax.inject.Inject

class EditUserService : DaggerIntentService("EditUserService") {
    @Inject
    lateinit var db:DataBase

    override fun onHandleIntent(intent: Intent?) {
        db.userDAO().update(listOf(intent!!.extras.get(USER) as User))
        stopSelf()
    }

//    override fun onBind(intent: Intent): IBinder? {
//        return null
//    }
//
//    @Override
//    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
//        db.userDAO().update(listOf(intent!!.extras.get(USER) as User))
//        stopSelf()
//        return START_NOT_STICKY
//    }
}
