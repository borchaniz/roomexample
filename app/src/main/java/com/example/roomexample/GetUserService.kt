package com.example.roomexample

import android.app.IntentService
import android.content.Intent
import android.content.Context
import android.util.Log
import dagger.android.DaggerIntentService
import javax.inject.Inject

const val RETURNED_USER = "com.example.roomexample.RETURNED_USER"
const val ACTION_GET_USER = "com.example.roomexample.RESPONSE"

class GetUserService : DaggerIntentService("GetUserService") {

    @Inject
    lateinit var db: DataBase

    override fun onHandleIntent(intent: Intent?) {
        val response = Intent()
        response.action = ACTION_GET_USER
        response.putExtra(RETURNED_USER, db.userDAO().getUser())
        sendBroadcast(response)
        stopSelf()
    }


}
