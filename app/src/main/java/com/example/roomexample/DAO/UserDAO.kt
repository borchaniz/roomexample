package com.example.roomexample.DAO

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query
import android.arch.persistence.room.Update
import com.example.roomexample.Entities.User

@Dao
interface UserDAO {
    @Query("select * from users")
    fun getAll():List<User>

    @Query("select * from users where id=1")
    fun getUser():User

    @Insert
    fun insert(user: User):Long

    @Update
    fun update(user:List<User>):Int
}