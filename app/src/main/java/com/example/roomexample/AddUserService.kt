package com.example.roomexample

import android.app.IntentService
import android.content.AsyncQueryHandler
import android.content.Intent
import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.os.ResultReceiver
import android.util.Log
import com.example.roomexample.Entities.User
import dagger.android.DaggerIntentService
import javax.inject.Inject


class AddUserService : DaggerIntentService("AddUserService") {

    @Inject
    lateinit var db: DataBase

    override fun onHandleIntent(intent: Intent?) {
        db.userDAO().insert(intent!!.extras.get(USER) as User)
        stopSelf()

    }


}
