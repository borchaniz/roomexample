package com.example.roomexample

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.os.ResultReceiver
import android.support.v4.content.LocalBroadcastManager
import android.util.Log
import android.widget.Button
import android.widget.EditText
import com.example.roomexample.Entities.User
import dagger.android.support.DaggerAppCompatActivity
import javax.inject.Inject

const val USER = "com.example.roomexample.USER"

class MainActivity : DaggerAppCompatActivity() {

    @Inject
    lateinit var db: DataBase


    private lateinit var name: EditText
    private lateinit var familyName: EditText
    private lateinit var password: EditText
    private lateinit var email: EditText
    private lateinit var setData: Button
    var user: User? = null
    lateinit var br:BroadcastReceiver


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initViews()
        br = object : BroadcastReceiver() {
            @Override
            override fun onReceive(context : Context?, intent: Intent?) {
                user = intent!!.extras.get(RETURNED_USER) as User
                setUIData(user!!)
            }
        }
        val filter = IntentFilter()
        filter.addAction(ACTION_GET_USER)
        registerReceiver(br, filter)
        startService(Intent(this@MainActivity, GetUserService::class.java))
        setData.setOnClickListener {
            if (user == null) {
                user = generateUser()
                startService(Intent(this@MainActivity, AddUserService::class.java).apply {
                    putExtra(USER, user!!)
                })
            } else {
                user = generateUser()
                startService(Intent(this@MainActivity, EditUserService::class.java).apply {
                    putExtra(USER, user!!)
                })
            }

        }



    }

    private fun initViews() {
        name = findViewById(R.id.name)
        familyName = findViewById(R.id.family_name)
        email = findViewById(R.id.email)
        password = findViewById(R.id.passwd)
        setData = findViewById(R.id.set_data)

    }

    private fun setUIData(user: User) {
        name.setText(user.name)
        familyName.setText(user.familyName)
        email.setText(user.email)
        password.setText(user.password)
    }

    private fun generateUser() = User(
            id = 1,
            name = this@MainActivity.name.text.toString(),
            familyName = this@MainActivity.familyName.text.toString(),
            email = this@MainActivity.email.text.toString(),
            password = this@MainActivity.password.text.toString())


    @Override
    override fun onDestroy() {
        super.onDestroy()
//        unregisterReceiver(br)
    }
}
