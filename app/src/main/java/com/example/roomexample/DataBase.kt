package com.example.roomexample

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context
import com.example.roomexample.DAO.UserDAO
import com.example.roomexample.Entities.User

@Database(entities = [User::class], version = 1)
abstract class DataBase : RoomDatabase() {
    abstract fun userDAO(): UserDAO

    companion object {
        private var instance: DataBase? = null
        fun getInstance(context: Context): DataBase? {
            if (instance == null) {
                synchronized(DataBase::class) {
                    instance = Room.databaseBuilder(context.applicationContext, DataBase::class.java, "room_example.db").build()
                }
            }
            return instance
        }

        fun destroyInstance() {
            instance = null
        }
    }


}