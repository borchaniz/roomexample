package com.example.roomexample.Entities

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import java.io.Serializable

@Entity (tableName = "users")
data class User (
        @PrimaryKey(autoGenerate = false) var id:Long?,
        @ColumnInfo(name="name") var name: String,
        @ColumnInfo(name="family_name") var familyName: String,
        @ColumnInfo(name="email") var email: String,
        @ColumnInfo(name="password") var password: String
): Serializable